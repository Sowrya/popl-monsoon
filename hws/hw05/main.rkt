
#lang racket

(define-syntax colist
  (syntax-rules ()
    [(colist sym terms ...) 
     (lambda () (list sym terms ...))]))

;;; unroll :: term? -> term?
(define (unroll t)
  (if (procedure? t) (t) t))
;;; hd :: term? -> symbol?
(define (hd t)
  (car (unroll t)))

;;; tl :: term? -> term?
(define (tl t)
  (cdr (unroll t)))


(define first-bisim
  (lambda (bisim-list)
    (car (car bisim-list))))


(define second-bisim
  (lambda (bisim-list)
    (car (cdr (car bisim-list)))))


(define search-sim-list
  (lambda (bisim-list t1 t2)
  (cond
    [(equal? t1 t2) #t]
    
    [(equal? bisim-list '()) #f]
    
    [(equal? (first-bisim bisim-list) t1) (if (equal? (second-bisim bisim-list) t2) #t (search-sim-list (cdr bisim-list) t1 t2))]
    
    [(equal? (first-bisim bisim-list) t2) (if (equal? (second-bisim bisim-list) t1) #t (search-sim-list (cdr bisim-list) t1 t2))]
    
    [else (search-sim-list (cdr bisim-list) t1 t2)])))


(define add-list
  (lambda (bisim-list t1 t2)
  (if (not (search-sim-list bisim-list t1 t2))
      (append bisim-list (list (list t1 t2)))
      bisim-list)))



(define exc-colist?
  (lambda (type-check)
    (and (not (list? type-check)) (list? (unroll type-check)))))

(define bisimilar-rec?

  (lambda (t1 t2 sim-list)

    (cond

      [(search-sim-list sim-list t1 t2) #t]
      
      [(equal? t1 t2) #t]
      
      [(or (eq? t1 '()) (eq? t2 '())) #f]
      
      
      [(and (exc-colist? (hd t1)) (exc-colist? (hd t2))) (if (and (bisimilar-rec? (hd t1) (hd t2) sim-list) (bisimilar-rec? (tl t1) (tl t2) sim-list)) #t #f) ]

      [(and (exc-colist? (hd t1)) (list? (hd t2))) (if (equal? (hd (hd t1)) (hd t2))
                                                       (bisimilar-rec? (append (tl (hd t1)) (tl t1)) (tl t2) (add-list sim-list t1 t2)) #f)]

      [(and (list? (hd t1)) (exc-colist? (hd t2))) (if (equal? (hd t1) (hd (hd t2)))
                                                       (bisimilar-rec? (tl t1) (append (tl (hd t2)) (tl t1)) (add-list sim-list t1 t2)) #f)]

      [(and (not (exc-colist? (hd t1))) (not (exc-colist? (hd t2)))) (if (equal? (hd t1) (hd t2))
                                                                         (bisimilar-rec? (tl t1) (tl t2) (add-list sim-list t1 t2))
                                                                         #f)])))


(define n-colist?
  (lambda (type-check)
    (list? (unroll type-check))))

(define (bisimilar? t1 t2)
  (if (and (n-colist? t1) (n-colist? t2) (not (or (eq? t1 '()) (eq? t2 '())))) (bisimilar-rec? t1 t2 '()) (raise-type-error)))



(provide bisimilar?)
(provide colist)
